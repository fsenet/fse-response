package com.fse.comm.gudid;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		
    "classpath:gudid1message-test.xml"
})
public class GUDID1Test {
	

	private RestTemplate restTemplate;
	
	public static final String url = "http://localhost:8183/fse-response-service/rest/response/postGUDID1";
	public static final String mType = "ACK1";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		publishGUDID1Messages();
	}
	
	private void publishGUDID1Messages() {
		String request = getInputString();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, request, String.class);
		System.out.println(httpresult);
	}
	
	private String getInputString() {
		if(mType.equals("ACK1")) {
			return getInputACK1String();
		} else {
			return getInputACK2String();
		}
	}

	private String getInputACK1String() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("GUDID_XML_20160830135255_1.tar.gz%3AFDATest-20160830-134901676-DdwD%40807900258_ZZFDATST=");
		/*sb.append("This MDN (Message Disposition Notification) was automatically built on\n");
		sb.append("Tue, 25 Jul 2016 23:36:26 GMT in response to a");
		sb.append(" message with id <20425118.41395790583689 JavaMail.John.ADAMS@ABC1234567> received on");
		sb.append(" 22FDATST on Tue, 25 Jul 2014 23:36:25 GMT.");
		sb.append(" unless stated otherwise, the message to which MDN applies");
		sb.append(" was successfully processed.");*/
		return sb.toString();
	}
	
	private String getInputACK2String() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("MessageId%3A+%3CFDATest-20160818-203704255-qvCy%40807900258_ZZFDATST%3E%0ACoreId%3A+ci1471552621208.3316469%40fdsuv05637_te1");
		sb.append("%0ADateTime+Receipt+Generated%3A+08-18-2016%2C+16%3A37%3A26%0AFile+Count%3A+1%0ADirectory+Count%3A+2%0A%0A%22CDRH+has+received+your+submission%22=");
		/**sb.append("MessageId: <20425118.41395790583689.ECS.Administrator@ecs>\n");
		sb.append("CoreId: ci1442341644213.1631122@fdsul05646_te1\n");
		sb.append("DateTime Receipt Generated: 07-28-2016, 14:28:10\n");
		sb.append(" 22FDATST on Tue, 28 Jul 2016 23:36:25 GMT.");
		sb.append("File Count: 1\nDirectory Count: 2\n\n");
		sb.append("CDRH has received your submission");**/
		return sb.toString();
	}


}
