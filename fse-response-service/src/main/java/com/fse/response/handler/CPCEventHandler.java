package com.fse.response.handler;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fse.comm.biz.response.FseCPCHelper;
import com.fse.comm.handler.FseEventHandler;
import com.fse.fsenet.server.services.messages.CPCResponseMessage;

@Component
public class CPCEventHandler implements FseEventHandler<CPCResponseMessage, String> {

	private static final Logger logger = LoggerFactory.getLogger(CPCEventHandler.class);

    @Autowired
	private FseCPCHelper fseCPCHelper;

	@Override
	public String process(String event, CPCResponseMessage cpcMessage) {
		logger.info("Event: " + event + "-->" + cpcMessage);

		try {
			fseCPCHelper.process(cpcMessage);
		} catch (RuntimeException e) {
			logger.error(ExceptionUtils.getFullStackTrace(e.getCause()));
			throw e;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}

		return "Success";
	}

}
