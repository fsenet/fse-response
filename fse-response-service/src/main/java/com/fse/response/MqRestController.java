package com.fse.response;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fse.comm.mq.QueueManager;
import com.fse.common.properties.DefaultProperties;

@Controller
@RequestMapping("/mq/*")
public class MqRestController {

    private static final Logger logger = LoggerFactory.getLogger(MqRestController.class);

	@Autowired
	private QueueManager queueManager;
	
	private boolean shutdown = false;
	private boolean started = true;
	
	@RequestMapping("/status")
    public @ResponseBody String getStatus() {
	    return doGetStatus();
    }

    static private boolean testMode = DefaultProperties.getBooleanProperty("TestMode", false);
    
    @RequestMapping("/start")
    public @ResponseBody String start() {
        logger.info("/start");
        try {
            if (!shutdown && !started) {
                queueManager.start();
                started = true;
            }
            else {
                return "Already started!";
            }
        }
        catch (RuntimeException e) {
            logger.error("/start", ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error("/start", ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        logger.info("/start completed");
        return doGetStatus();
    }

    @RequestMapping("/stop")
    public @ResponseBody String stop() {
        logger.info("/stop");
        try {
            if (!shutdown && started) {
                logger.info("stopping queues!");
                queueManager.stop();
                started = false;
            }
            else {
                return "Already stopped!";
            }
        }
        catch (RuntimeException e) {
            logger.error("/stop", ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error("/stop", ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        logger.info("/stop completed");
        return doGetStatus();
    }

    @RequestMapping("/reset")
    public @ResponseBody String reset() {
        logger.info("/reset");
        logger.info("/reset response queue connection");
        try {
            queueManager.init();
        }
        catch (RuntimeException e) {
            logger.error("/reset", ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error("/reset", ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "response mq resetted";
        
    }

    @RequestMapping("/shutdown")
    public @ResponseBody String shutdown() {
        logger.info("/shutdown");
        try {
        	if (!shutdown) {
        		queueManager.terminate();
                shutdown = true;
                started = false;
        	}
            if (testMode) {
                Thread.sleep(10*1000);
            }
        }
        catch (RuntimeException e) {
            logger.error("/stop", ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error("/stop", ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }

        logger.info("/shutdown completed!");
        return doGetStatus();
    }

    private String doGetStatus() {
        return shutdown ? "shutdown" : started ? "started" : "stopped";        
    }
}
