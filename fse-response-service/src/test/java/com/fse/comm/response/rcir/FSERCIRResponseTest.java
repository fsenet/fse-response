package com.fse.comm.response.rcir;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class FSERCIRResponseTest {
	
	private RestTemplate restTemplate;
	
	public static final String url = "http://localhost:8086/fse-response-service/rest/response/postRCIR";


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}


	@Test
	public void test() {
		postRCIRMessages();
	}
	
	private void postRCIRMessages() {
		String request = getInputString();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, request, String.class);
		System.out.println(httpresult);
		
	}
	
	private String getInputString() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<catalogueItemReference>");
		sb.append("<dataSource>0049485000006</dataSource>");
		//sb.append("<gtin>01004898825003</gtin>");
		sb.append("<gtin>00049485754909</gtin>");
		sb.append("<targetMarketCountryCode>840</targetMarketCountryCode>");
		sb.append("</catalogueItemReference>");
		return sb.toString();
	}

}
