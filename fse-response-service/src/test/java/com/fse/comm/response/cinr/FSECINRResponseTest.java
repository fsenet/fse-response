package com.fse.comm.response.cinr;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class FSECINRResponseTest {
	

	private RestTemplate restTemplate;
	
	public static final String url = "http://localhost:8183/fse-response-service/rest/response/postCINR";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		postCINRMessages();
	}
	
	private void postCINRMessages() {
		String request = getInputString();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, request, String.class);
		System.out.println(httpresult);
		
	}
	
	private String getInputString() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		sb.append("<gs1_response:gS1ResponseMessage xmlns:gs1_response=\"urn:gs1:gdsn:gs1_response:xsd:3\" xmlns:sh=\"http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:gs1:gdsn:gs1_response:xsd:3 http://www.gs1globalregistry.net/3.1/schemas/gs1/gdsn/GS1Response.xsd\">");
		sb.append("<sh:StandardBusinessDocumentHeader><sh:HeaderVersion>1.0</sh:HeaderVersion><sh:Sender><sh:Identifier Authority=\"GS1\">0838016003001</sh:Identifier></sh:Sender><sh:Receiver><sh:Identifier Authority=\"GS1\">0850522001050</sh:Identifier></sh:Receiver><sh:DocumentIdentification><sh:Standard>GS1</sh:Standard><sh:TypeVersion>3.1</sh:TypeVersion><sh:InstanceIdentifier>cinr.LGAUF5410000414</sh:InstanceIdentifier><sh:Type>gS1Response</sh:Type><sh:CreationDateAndTime>2016-10-27T10:54:10</sh:CreationDateAndTime></sh:DocumentIdentification><sh:BusinessScope><sh:Scope><sh:Type>GDSN</sh:Type><sh:InstanceIdentifier>cinr.LGAUF5410000414</sh:InstanceIdentifier><sh:CorrelationInformation><sh:RequestingDocumentInstanceIdentifier>CIHW-30606638</sh:RequestingDocumentInstanceIdentifier></sh:CorrelationInformation></sh:Scope></sh:BusinessScope></sh:StandardBusinessDocumentHeader><gS1Response><originatingMessageIdentifier><entityIdentification>CIHW-30606638</entityIdentification><contentOwner><gln>0850522001050</gln></contentOwner></originatingMessageIdentifier><receiver>0838016003001</receiver><sender>0850522001050</sender><transactionResponse><transactionIdentifier><entityIdentification>CIHW.1477582967094.145872_0727351000007.43</entityIdentification></transactionIdentifier><responseStatusCode>ACCEPTED</responseStatusCode></transactionResponse></gS1Response></gs1_response:gS1ResponseMessage>");
		return sb.toString();
	}



}
