<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>response service</title>
<link href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.structure.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.theme.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/jqueryui/1.8/themes/base/jquery.ui.tabs.css" />" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/jquery/jquery-2.1.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/jqueryform/2.8/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/jquery-ui-1.11.4/jquery-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/json2.js" />"></script>

<link href="<c:url value="/resources/form.css" />" rel="stylesheet"    type="text/css" />
<link href="<c:url value="/resources/fseStyle.css" />" rel="stylesheet"    type="text/css" />

<!-- jqGrid -->
<script type="text/ecmascript" src="resources/jqGrid/js/i18n/grid.locale-en.js"></script>
<script type="text/ecmascript" src="resources/jqGrid/js/jquery.jqGrid.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="resources/jqGrid/css/ui.jqgrid.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />

<style type="text/css">

.command {
	text-decoration: underline;
}
	
</style>

</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
</head>
<body>

    <script type="text/javascript">
        
    	var restRoot='mq';
        var statusColor = [];
        statusColor['started'] = 'blue';
        statusColor['stopped'] = 'red';
        statusColor['shutdown'] = 'pink';
        var rootUrl = window.location.href;

        var statusUrl = rootUrl.concat(restRoot.concat("/status"));

        $(document).ready(function() {

            refreshContents();

            $("#serverTime").click(function() {
                $("#serverTime").hide().fadeIn('slow');
            });

            $("#status").click(function() {
                status = getStatus();
            });

            $(".command").click(function() {
                var cmd = $(this).attr("cmd");
                var url = rootUrl.concat(cmd);
                console.log("starting ".concat(cmd));
                $(document.body).addClass('busy');  
                $.ajax({
                    url : url
                }).then(function(data) {
                    refreshContents(); 
                    console.log("data ".concat(cmd).concat(" -- ").concat(data));
                    $(document.body).removeClass('busy');
                });
                console.log("done ".concat(cmd));
            });
            
            $('.command').hover(function() {
                $(this).css('cursor','pointer');
            });
        });

        function refreshContents() {
            getStatus();
        }

        function getStatus() {
            var status;
            var elem;
            $.ajax({
                url : statusUrl
            }).then(function(data) {
                elem = document.getElementById("statusData");
                elem.innerHTML = data;
                elem.style.color = statusColor[data];
                $("#statusData").hide().fadeIn('slow');
                checkShutdown(data);
            });
        };

        function checkShutdown(data) {
        	var result = "shutdown".localeCompare(data);
        	
        	if (result == 0) {
        		$(".command").addClass("not-active");
        	}	
        }

        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    </script>

	<h1>
		Response Server Home!
	</h1>
    <hr>
    <p id="status">
        <a class="titleText">Queue Status -- </a><a id="statusData"></a>
    </p>

    <div>
        <ul>
            <li><a class="command" cmd="mq/start">Start dequeue</a></li>
            <li><a class="command" cmd="mq/stop">Stop dequeue</a></li>
            <li><a class="command" cmd="mq/shutdown">Shutdown response service</a></li>
        </ul>
    </div>

	<P>  The time on the server is ${serverTime}. </P>
</body>
</html>
