package com.fse.response.handler;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fse.comm.biz.response.FseGUDID2Helper;
import com.fse.comm.handler.FseEventHandler;
import com.fse.fsenet.server.services.messages.GUDIDMessage;

@Component
public class GUDID2EventHandler implements FseEventHandler<GUDIDMessage, String> {
	private static final Logger logger = LoggerFactory.getLogger(GUDID2EventHandler.class);

    @Autowired
	private FseGUDID2Helper fseGUDID2Helper;



	@Override
	public String process(String event, GUDIDMessage gudidMessage) throws Exception {
		logger.info("Event: " + event + "-->" + gudidMessage);

		try {
			fseGUDID2Helper.process(gudidMessage);
		} catch (RuntimeException e) {
			logger.error(ExceptionUtils.getFullStackTrace(e.getCause()));
			throw e;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}

		return "Success";
	}

}
