package com.fse.response.handler;

import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fse.comm.biz.response.FseRCIRHelper;
import com.fse.comm.handler.FseEventHandler;
import com.fse.fsenet.server.services.messages.FsePublicationMessage;
import com.fse.fsenet.server.services.messages.RCIResponseMessage;

@Component
public class RCIREventHandler implements FseEventHandler<RCIResponseMessage, String> {

	private static final Logger logger = LoggerFactory.getLogger(RCIREventHandler.class);

    @Autowired
	private FseRCIRHelper fseRCIRHelper;

	@Autowired
	private PublicationMessageSender publicationMessageSender;

	@Override
	public String process(String event, RCIResponseMessage rcirMessage) {
		logger.info("Event: " + event + "-->" + rcirMessage);

		try {
			List<FsePublicationMessage> publicationMessages = fseRCIRHelper.process(rcirMessage);

			if (publicationMessages != null) {
				logger.info("Sending the Publication Message for " + rcirMessage+ " to the Publication Queue");
				publicationMessageSender.sendMessages(publicationMessages);
				logger.info("Sent the Publication Message for " + rcirMessage+ " to the Publication Queue");
			} else {
				logger.info("Unable to Send the Publication Message(null) for " + rcirMessage+ " to the Publication Queue");
			}
		} catch (RuntimeException e) {
			logger.error(ExceptionUtils.getFullStackTrace(e.getCause()));
			throw e;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}

		return "Success";
	}

}
