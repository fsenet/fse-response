package com.fse.comm.gudid;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class GUDID3Test {
	private RestTemplate restTemplate;

	public static final String url = "http://localhost:8280/fse-response-service/rest/response/postGUDID3";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		sendGUDID3Messages();
	}
	
	private void sendGUDID3Messages() {
		String inputString = getunParseableErrorMessage();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, inputString, String.class);
		System.out.println(httpresult);
	}
	
	private String getunParseableErrorMessage() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<p>");
		sb.append("Unidentified or unparseable submission type ");
		sb.append("ci1442341644213.1631122@fdsul05646_te1");
		sb.append("</p>");
		sb.append("</body>");
		sb.append("</html>");
		return sb.toString();
	}

}
