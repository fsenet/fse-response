package com.fse.comm.gudid;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		
    "classpath:gudid1message-test.xml"
})
public class GUDID2Test {
	private RestTemplate restTemplate;

	public static final String url = "http://localhost:8280/fse-response-service/rest/response/postGUDID2";
	public static final String mType = "Success1";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		publishGUDID2Messages();
	}
	
	private void publishGUDID2Messages() {
		String request = getInputString();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, request, String.class);
		System.out.println(httpresult);
	}
	
	private String getInputString() {
		if(mType.equals("Success")) {
			return getInputACK3SuccessString();
		} else {
			return getInputACK3FailureString();
		}
	}

	private String getInputACK3SuccessString() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<submission>\n");
		sb.append("<coreId>ci1442341644213.1631122@fdsul05646_te1</coreId>\n");
		sb.append("<batchId>3</batchId>\n");
		sb.append("<dateEntered>09-15-2015, 14:28:10</dateEntered>\n");
		sb.append("<numReportFailed>0</numReportFailed>\n");
		sb.append("<numReportPassed>1</numReportPassed>\n");
		sb.append("<report id='00733132612468'>");
		sb.append("<status>passed</status>");
		sb.append("</report>");
		sb.append("</submission>");
		return sb.toString();
	}
	
	private String getInputACK3FailureString() {
		StringBuilder sb = new StringBuilder(300);
		sb.append("<submission>\n");
		sb.append("<coreId>ci1442341644213.1631122@fdsul05646_te1</coreId>\n");
		sb.append("<batchId>3</batchId>\n");
		sb.append("<dateEntered>09-15-2016, 14:28:10</dateEntered>\n");
		sb.append("<numReportFailed>2</numReportFailed>\n");
		sb.append("<numReportPassed>0</numReportPassed>\n");
		sb.append("<report id='00733132612468'>");
		sb.append("<status>failed</status>");
		sb.append("</report>");
		sb.append("<failure>");
		sb.append("<reportId>00733132612468</reportId>");
		sb.append("<detail>");
		sb.append("<section />");
		sb.append("<errorMessage>Step 5. The GMDN Code 38572 is obsolete; contact the GMDN Agency (www.gmdnagency.com) to obtain a valid GMDN PT Code.</errorMessage>");
		sb.append("<messageValue />");
		sb.append("<xPath />");
		sb.append("</detail>");
		sb.append("</failure>");
		sb.append("<failure>");
		sb.append("<reportId>00733132612468</reportId>");
		sb.append("<detail>");
		sb.append("<section />");
		sb.append("<errorMessage>The GMDN Code 38572 is obsolete; contact the GMDN Agency (www.gmdnagency.com) to obtain a valid GMDN PT Code.</errorMessage>");
		sb.append("<messageValue />");
		sb.append("<xPath />");
		sb.append("</detail>");
		sb.append("</failure>");

		sb.append("</submission>");
		return sb.toString();
	}


}
