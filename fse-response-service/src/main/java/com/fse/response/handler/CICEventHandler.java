package com.fse.response.handler;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fse.comm.biz.response.FseCICHelper;
import com.fse.comm.handler.FseEventHandler;
import com.fse.fsenet.server.services.messages.CICResponseMessage;

@Component
public class CICEventHandler implements FseEventHandler<CICResponseMessage, String> {

	private static final Logger logger = LoggerFactory.getLogger(CICEventHandler.class);

    @Autowired
	private FseCICHelper fseCICHelper;

	@Override
	public String process(String event, CICResponseMessage cicMessage) {
		logger.info("Event: " + event + "-->" + cicMessage);

		try {
			fseCICHelper.process(cicMessage);
		} catch (RuntimeException e) {
			logger.error(ExceptionUtils.getFullStackTrace(e.getCause()));
			throw e;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}

		return "Success";
	}

}
