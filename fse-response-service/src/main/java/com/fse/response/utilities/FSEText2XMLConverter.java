package com.fse.response.utilities;

import java.io.StringReader;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.fse.fsenet.server.services.messages.FSEGUDIDDateFormatMasterTypes;

public class FSEText2XMLConverter {
	
	public static final String ACK2CoreStartString = "CoreId:";
	public static final String ACK2CoreEndString = "DateTime";
	
	public static final String ACK2MessageStartString = "MessageId: <";
	public static final String ACK2MessageEndString = "@";
	
	public static final String ACK2DateEnteredStartString = "DateTime Receipt Generated:";
	public static final String ACK2DateEnteredEndString = "File";
	
	public static final String ACK1MessageStartString = ":";
	public static final String ACK1MessageEndString = ">>";
	
	public static final String ACK1DateEnteredStartString = "built on";
	public static final String ACK1DateEnteredEndString = "in";
	
	public static final String ACK3CoreStartString = "submission type";
	public static final String ACK3CoreEndString = "</p>";
	
	
	
	public Document convertACK12TextGUDID2XML(String message) throws Exception {
		Document document =  null;
		String value = message;
		String coreID = getNextString(message, ACK2CoreStartString, ACK2CoreEndString);
		if(coreID != null && coreID.length() > 0) {
			coreID = coreID.trim();
		}
		String messageID = "";
		String filename = "";
		String dateStr = "";
		FSEGUDIDDateFormatMasterTypes gudidDateFormatTypes = new FSEGUDIDDateFormatMasterTypes();
		if (coreID != null && coreID.length() > 0) {
			messageID = getNextString(message, ACK2MessageStartString, ACK2MessageEndString); 
			dateStr = getNextString(message, ACK2DateEnteredStartString, ACK2DateEnteredEndString);
			if (dateStr != null && dateStr.length() > 0) {
				Date dt = gudidDateFormatTypes.convertGUDIDDate2Date(dateStr, gudidDateFormatTypes.getGUDIDACK2DateType());
				dateStr = dt != null?dt.toString():"";
			}
		} else {
			if (message == null || message.length() == 0) {
				message = value;
			}
			filename = getParsed(message,":", 0);
			messageID = getParsed(message,":", 1);
			messageID = messageID.replaceAll("=", "");
			messageID = getParsed(messageID,"@", 0);
			dateStr = getNextString(message, ACK1DateEnteredStartString, ACK1DateEnteredEndString);
			if (dateStr != null && dateStr.length() > 0) {
				Date dt = gudidDateFormatTypes.convertGUDIDDate2Date(dateStr, gudidDateFormatTypes.getGUDIDACK1DateType());
				dateStr = dt != null?dt.toString():"";
			}
		}
		if ( (messageID != null && messageID.length() > 0) ||
				(coreID != null && coreID.length() > 0) ||
				(dateStr != null && dateStr.length() > 0)) {
			String xmlvalue = Convert2XMLString(coreID, messageID, dateStr, filename);
			document = getXMLFromInput(xmlvalue);
		}
		return document;
	}
	
	public Document convertACK3TextGUDID2XML(String message) throws Exception {
		Document document =  null;
		String coreID = getNextString(message, ACK3CoreStartString, ACK3CoreEndString);
		if(coreID != null && coreID.length() > 0) {
			coreID = coreID.trim();
		}
		if (coreID != null && coreID.length() > 0) {
			String value = Convert2XMLString(coreID, "", "", "");
			document = getXMLFromInput(value);
		}
		return document;
	}
	

	
	private String getNextString(String text, String checkString, String endString) {
		String value = "";
		try {
			int location = text.indexOf(checkString);
			location = location + checkString.length();
			int endlocation = text.indexOf(endString);
			value = (String) text.subSequence(location, endlocation);
			if(value != null && value.length() > 0) {
				value = value.trim();
			}
		} catch(Exception ex) {
			value = "";
		}
		return value;
	}
	
	private String getParsed(String text, String parserString, int loc) {
		String val[] = text.split(parserString);
		return val[loc];
	}
	
	private String Convert2XMLString(String coreID, String messageID, String dataTimeStr, String filename) {
		StringBuilder xmlStr = new StringBuilder(300);
		xmlStr.append("<submission>");
		xmlStr.append("<fileId>");
		xmlStr.append(filename);
		xmlStr.append("</fileId>");
		xmlStr.append("<messageId>");
		xmlStr.append(messageID);
		xmlStr.append("</messageId>");
		if (coreID != null && coreID.length() > 0) {
			xmlStr.append("<coreId>");
			xmlStr.append(coreID);
			xmlStr.append("</coreId>");
		}
		xmlStr.append("<dateEntered>");
		xmlStr.append(dataTimeStr);
		xmlStr.append("</dateEntered>");
		xmlStr.append("</submission>");
		return xmlStr.toString();
	}
	
    public Document getXMLFromInput(String message) {
    	try {
    	// message = message.trim().replaceFirst("^([\\W]+)<","<");
			int endIndex = message.lastIndexOf('>');
			if (endIndex + 1 <= message.length()) {
				message = message.substring(0, endIndex + 1);
			}
		    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    InputSource is = new InputSource();
		    is.setCharacterStream(new StringReader(message));
			return db.parse(is);
    	}
    	catch (Exception e) {
    		throw new RuntimeException(e);
    	}
    }


}
