package com.fse.comm.response.cic;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

public class FSECICResponseTest {
	private RestTemplate restTemplate;

	public static final String url = "http://localhost:8183/fse-response-service/rest/response/postCIC";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void test() {
		sendCICResponseMessages();
	}

	private void sendCICResponseMessages() {
		String inputString = getunParseableErrorMessage();
		restTemplate = new RestTemplate();
		String httpresult = restTemplate.postForObject(url, inputString, String.class);
		System.out.println(httpresult);
	}
	
	private String getunParseableErrorMessage() {
		StringBuilder sb = new StringBuilder(300);
		return sb.toString();
	}


}
