package com.fse.response.rest;

import java.io.ByteArrayInputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.fse.comm.biz.response.FseCICHelper;
import com.fse.comm.biz.response.FseCINRHelper;
import com.fse.comm.biz.response.FseCPCHelper;
import com.fse.comm.biz.response.FseGUDID1Helper;
import com.fse.comm.biz.response.FseGUDID2Helper;
import com.fse.comm.biz.response.FseGUDID3Helper;
import com.fse.comm.biz.response.FseRCIRHelper;
import com.fse.common.properties.DefaultProperties;
import com.fse.fsenet.server.services.messages.CICResponseMessage;
import com.fse.fsenet.server.services.messages.CINRResponseMessage;
import com.fse.fsenet.server.services.messages.CPCResponseMessage;
import com.fse.fsenet.server.services.messages.GUDIDMessage;
import com.fse.fsenet.server.services.messages.RCIResponseMessage;
import com.fse.response.handler.CICMessageSender;
import com.fse.response.handler.CINRMessageSender;
import com.fse.response.handler.CPCMessageSender;
import com.fse.response.handler.GUDID1MessageSender;
import com.fse.response.handler.GUDID2MessageSender;
import com.fse.response.handler.GUDID3MessageSender;
import com.fse.response.handler.RCIRMessageSender;
import com.fse.response.utilities.FSEText2XMLConverter;

@RestController
@RequestMapping("/rest/response")
public class ResponseRestController {

	private static final Logger logger = Logger.getLogger(ResponseRestController.class);

	@Autowired
	private FseCICHelper fseCICHelper;

	@Autowired
	private FseCINRHelper fseCINRHelper;

	@Autowired
	private FseRCIRHelper fseRCIRHelper;

	@Autowired
	private FseCPCHelper fseCPCHelper;

	@Autowired
	private FseGUDID1Helper fseGUDID1Helper;

	@Autowired
	private FseGUDID2Helper fseGUDID2Helper;

	@Autowired
	private FseGUDID3Helper fseGUDID3Helper;

	@Autowired
	private CICMessageSender cicMessageSender;

	@Autowired
	private CINRMessageSender cinrMessageSender;

	@Autowired
	private RCIRMessageSender rcirMessageSender;

	@Autowired
	private CPCMessageSender cpcMessageSender;

	@Autowired
	private GUDID1MessageSender gudid1MessageSender;

	@Autowired
	private GUDID2MessageSender gudid2MessageSender;

	@Autowired
	private GUDID3MessageSender gudid3MessageSender;

	@RequestMapping(value = "/status", method = RequestMethod.GET)
    public @ResponseBody String getStatus() {
	    return "alive";
	}

	static private boolean testMode = DefaultProperties.getBooleanProperty("TestMode", false);

	@RequestMapping(value = "/postCIC", method = RequestMethod.POST)
    public @ResponseBody String postCIC(@RequestBody String message) {
		try {
			Document document = getXMLFromInput(message);
			List<CICResponseMessage> cicMessages = fseCICHelper.getMessageFromXML(document);
			cicMessageSender.sendMessages(cicMessages);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

	@RequestMapping(value = "/postCINR", method = RequestMethod.POST)
    public @ResponseBody String postCINR(@RequestBody String message) {
		try {
			Document document = getXMLFromInput(message);
			List<CINRResponseMessage> cinrMessages = fseCINRHelper.getMessageFromXML(document);
			cinrMessageSender.sendMessages(cinrMessages);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

//	@RequestMapping(value = "/fakeCICR/{docId}", method = RequestMethod.GET)
//    public @ResponseBody String fakeCICR(@PathVariable("docId") final String docId) {
//		try {
//			CICResponseMessage cicrMessage = new CICResponseMessage();
//			fseCICHelper.process(cicrMessage);
//		} catch (RuntimeException e) {
//            logger.error(ExceptionUtils.getFullStackTrace(e));
//            throw e;
//        }
//        catch (Exception e) {
//            logger.error(ExceptionUtils.getFullStackTrace(e));
//            throw new RuntimeException(e);
//        }
//        return "success";
//	}
	
	@RequestMapping(value = "/fakeCINR/{docId}", method = RequestMethod.GET)
    public @ResponseBody String fakeCINR(@PathVariable("docId") final String docId) {
		try {
			CINRResponseMessage cinrMessage = new CINRResponseMessage(new Date(), docId, null);
			fseCINRHelper.process(cinrMessage);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}
	
	@RequestMapping(value = "/fakeRCIR/{dataSource}/{gtin}/{targetMarketCountryCode}", method = RequestMethod.GET)
    public @ResponseBody String fakeRCIR(@PathVariable("dataSource") final String dataSource,
    		@PathVariable("gtin") final String gtin,
    		@PathVariable("targetMarketCountryCode") final String targetMarketCountryCode) {
		try {
			List<RCIResponseMessage> rcirMessages = new ArrayList<>();
			RCIResponseMessage rcirMessage = new RCIResponseMessage(dataSource, gtin, targetMarketCountryCode);
			rcirMessages.add(rcirMessage);
			rcirMessageSender.sendMessages(rcirMessages);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}
	
	@RequestMapping(value = "/postRCIR", method = RequestMethod.POST)
	public @ResponseBody String postRCIR(@RequestBody String message) {
		try {
			Document document = getXMLFromInput(message);
			List<RCIResponseMessage> rcirMessages = fseRCIRHelper.getMessageFromXML(document);
			rcirMessageSender.sendMessages(rcirMessages);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

	@RequestMapping(value = "/postCPC", method = RequestMethod.POST)
    public @ResponseBody String postCPC(@RequestBody String message) {
		try {
			Document document = getXMLFromInput(message);
			List<CPCResponseMessage> cpcMessages = fseCPCHelper.getMessageFromXML(document);
			cpcMessageSender.sendMessages(cpcMessages);
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

	@RequestMapping(value = "/postGUDID1", method = RequestMethod.POST)
    public @ResponseBody String postGUDID1(@RequestBody String message) {
		try {
			logger.info("gudidmessage: Reeived ACK1/ACK2 response :"+message);
			logger.info("gudidmessage: Before decoding ACK1/ACK2 response :"+message);
			String xmlString = URLDecoder.decode(message);
			logger.info("gudidmessage:After decoding ACK1/ACK2 response :"+xmlString);
			FSEText2XMLConverter ack12Converter = new FSEText2XMLConverter();
			Document document = ack12Converter.convertACK12TextGUDID2XML(xmlString);
			if (document != null) {
				logger.info("gudidmessage: processing ACK1/ACK2 response :"+document.toString());
				List<GUDIDMessage> gudidMessages = fseGUDID1Helper.getMessageFromXML(document);
				gudid1MessageSender.sendMessages(gudidMessages);
			} else {
				logger.error("gudidmessage: unable to parse the message :"+message);
			}
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}
	
	@RequestMapping(value = "/postGUDID2", method = RequestMethod.POST, produces=MediaType.APPLICATION_XML_VALUE)
    public @ResponseBody String postGUDID2(@RequestBody String message) {
		try {
			logger.info("gudidmessage: processing ACK3 response :"+message);
			String xmlString = URLDecoder.decode(message);
			logger.info("gudidmessage: After decoding ACK3 response :"+xmlString);
			FSEText2XMLConverter ack12Converter = new FSEText2XMLConverter();
			Document document = ack12Converter.getXMLFromInput(xmlString);
			if (document != null) {
				logger.info("gudidmessage: processing ACK3 response :"+document.toString());
				List<GUDIDMessage> gudidMessages = fseGUDID2Helper.getMessageFromXML(document);
				gudid2MessageSender.sendMessages(gudidMessages);
			} else {
				logger.error("gudidmessage: unable to parse the ACK3 message :"+message);
			}
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

	@RequestMapping(value = "/postGUDID3", method = RequestMethod.POST)
    public @ResponseBody String postGUDID3(@RequestBody String message) {
		try {
			FSEText2XMLConverter ack3Converter = new FSEText2XMLConverter();
			Document document = ack3Converter.convertACK3TextGUDID2XML(message);
			if (document != null) {
				logger.info("gudidmessage: processing ACK3 unparseable error response :"+document.toString());
				List<GUDIDMessage> gudidMessages = fseGUDID3Helper.getMessageFromXML(document);
				gudid3MessageSender.sendMessages(gudidMessages);
			} else {
				logger.error("gudidmessage: unable to parse ACK3 unparseable error message :"+message);
			}
		} catch (RuntimeException e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw e;
        }
        catch (Exception e) {
            logger.error(ExceptionUtils.getFullStackTrace(e));
            throw new RuntimeException(e);
        }
        return "success";
	}

    private Document getXMLFromInput(String message) throws Exception {
		InputSource input = new InputSource(new ByteArrayInputStream(message.getBytes("utf-8")));
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(input);
		return document;
    }
    
}
