package com.fse.comm.pubdelete;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fse.comm.biz.client.FSEPublicationDeleteClient;
import com.fse.fsenet.server.define.DATA_TYPE;
import com.fse.fsenet.server.services.messages.AddtionalAttributes;
import com.fse.fsenet.server.services.messages.PublicationMessage;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:publication-delete-test.xml"
})
public class PublicationDeleteTest {
	
	@Autowired
	FSEPublicationDeleteClient fsePublicationDeleteClient;
	
	Long pyId;
	Long tpyId;
	String targetId;
	Long prdId;
	String withdrawReason;

	@Test
	public void testPubDelete() {
		initializeData();
		List<PublicationMessage> messages = new ArrayList<PublicationMessage>();
		PublicationMessage pubmsg = new PublicationMessage();
		pubmsg.setPyId(pyId);
		pubmsg.setTpyId(tpyId);
		pubmsg.setTargetId(targetId);
		pubmsg.setPrdId(prdId);
		pubmsg.setTransactionType("PUBLICATION");
		pubmsg.setSourceTpyId(0);
		pubmsg.setSourceTraget("0");
		pubmsg.setAutoPublication(true);
		
		List<AddtionalAttributes> addtionalAttributesList = new ArrayList<AddtionalAttributes>();
		AddtionalAttributes aAttribute = new AddtionalAttributes("ACTION_DETAILS", DATA_TYPE.STRING, withdrawReason);
		addtionalAttributesList.add(aAttribute);
		pubmsg.setAddtionalAttributes(addtionalAttributesList);
		
		messages.add(pubmsg);
		doPublicationDelete(messages);

	}
	
	private void doPublicationDelete(List<PublicationMessage> pubmessage) {
		fsePublicationDeleteClient.asyncRequest(pubmessage);

	}
	
	private void initializeData() {
		pyId = 123213L;
		tpyId = 12321313L;
		targetId = "";
		prdId = 3243242143L;
		withdrawReason = "";
		
	}

}
