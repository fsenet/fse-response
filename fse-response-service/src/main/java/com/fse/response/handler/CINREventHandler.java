package com.fse.response.handler;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fse.comm.biz.response.FseCINRHelper;
import com.fse.comm.handler.FseEventHandler;
import com.fse.fsenet.server.services.messages.CINRResponseMessage;

@Component
public class CINREventHandler implements FseEventHandler<CINRResponseMessage, String> {

	private static final Logger logger = LoggerFactory.getLogger(CINREventHandler.class);

    @Autowired
	private FseCINRHelper fseCINRHelper;

	@Override
	public String process(String event, CINRResponseMessage cinrMessage) {
		logger.info("Event: " + event + "-->" + cinrMessage);

		try {
			fseCINRHelper.process(cinrMessage);
		} catch (RuntimeException e) {
			logger.error(ExceptionUtils.getFullStackTrace(e.getCause()));
			throw e;
		} catch (Exception e) {
			logger.error(ExceptionUtils.getFullStackTrace(e));
			throw new RuntimeException(e);
		}

		return "Success";
	}

}
